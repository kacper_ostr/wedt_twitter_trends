import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.StreamingContext._
import org.apache.log4j.Logger
import org.apache.log4j.Level
import scala.io.Source
import org.htmlcleaner.HtmlCleaner
import com.datastax.spark.connector.SomeColumns
import org.joda.time.{ DateTimeZone, DateTime }
import com.datastax.spark.connector.streaming._
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import scala.io.Source
import java.net.URL
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.collection.immutable.MapLike

object MainClass {
  def main(args: Array[String]) {

    val sparkConf = new SparkConf().setAppName("TwitterPopularTags").setMaster("local[*]")
      .set("spark.cassandra.connection.host", "localhost")


    //przniesc do jakis Utilsow
    def getHeadlinesFromUrl(url: String, tag: String): String = {
      val cleaner = new HtmlCleaner
      val props = cleaner.getProperties
      try {
        val address = new URL(url)
        val rootNode = cleaner.clean(address)
        val elements = rootNode.getElementsByName(tag, true)
        var toReturn = ""
        for (elem <- elements) {
          toReturn = elem.getText.toString()
        }
        return toReturn
      } catch {
        case e: Exception => return ""
      }
    }

    def now(time: Time): String =
      new DateTime(time.milliseconds, DateTimeZone.UTC).toString("yyyyMMddHH:mm:ss.SSS")

    def createSchema(): Unit = {
      CassandraConnector(sparkConf).withSessionDo { session =>
        session.execute(s"DROP KEYSPACE IF EXISTS twitter_trends")
        session.execute(s"CREATE KEYSPACE IF NOT EXISTS twitter_trends WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1 }")
        session.execute(s"""
                 CREATE TABLE IF NOT EXISTS twitter_trends.categories (
                    category text,
                    amount int,
                    PRIMARY KEY(category)
                )
               """)
      }
    }
    createSchema()

    Logger.getLogger("org").setLevel(Level.ERROR)

    //wczytywac jako parametry run-u
    System.setProperty("twitter4j.oauth.consumerKey", "15CfOibuBVakMCPldanuHMKoi")
    System.setProperty("twitter4j.oauth.consumerSecret", "zJvvWWrZEfEffrvrFKYZTpPApMrxRRKasr1mvtNOGUfnB4zPMt")
    System.setProperty("twitter4j.oauth.accessToken", "2855216086-eyqkzN27YIpI6Qkwj8TLHzrXrywLueU6BZ1Ndkf")
    System.setProperty("twitter4j.oauth.accessTokenSecret", "Xu32qXz4h5Q2nTAxxt4kC1qb0ClJnyOvOnQDYlaxIwAII")

    var filters = new Array[String](2)
    filters(0) = "language=en"
    filters(1) = "filter_level=medium"

    val sc = new SparkContext(sparkConf)

    val ssc = new StreamingContext(sparkConf, Seconds(1))

    val stream = TwitterUtils.createStream(ssc, None)

    val transform = (cruft: String) => cruft

    /** tutaj trzeba zaimplementowac algorytm */

    val tweetText = stream.filter( _.getLang.equals("en") )
    .map(status =>
      {                        
        //zwracana lista kategorii
        var categories = new ListBuffer[String]()

        if (status.getText != null && status.getText.length() > 0) {
          //odpytanie solr/elasticsearch
          categories.+=(status.getText)
        }
        if (status.getHashtagEntities.length > 0) {
          //poki co to odpytanie solr-a/elastic-a
          for (value <- status.getHashtagEntities) {
            categories.+=(value.getText)
          }
        }
        if (status.getURLEntities.length > 0) {
          for (value <- status.getURLEntities) {
            categories.+=(getHeadlinesFromUrl(value.getDisplayURL, "h1"))
            categories.+=(getHeadlinesFromUrl(value.getDisplayURL, "h2"))
          }
        }
        //return
        categories
      })

    val pairs = tweetText.map(text =>
      {
        val collection = text.asInstanceOf[ListBuffer[String]]
        val convrt = collection.groupBy(w => w).mapValues(_.size).map(identity)
        val max = convrt.maxBy(_._2)
        max
      })

    //pairs.saveToCassandra("twitter_trends", "categories", SomeColumns("category", "amount"))

     pairs.print()
      
    ssc.checkpoint("./checkpoint")
    ssc.start()
    ssc.awaitTermination()
  }
}